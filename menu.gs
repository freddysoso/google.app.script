Array.prototype.findIndex = function(search)
{
  if(search == "") return -1;
  for(var t = 0; t < this.length; t++)
  {
    if(this[t].search(search) >=0)
    {break}
  }
  return t-1;
}

function onOpen() {
  SpreadsheetApp.getUi()
  .createMenu('Cereb')
  .addSubMenu(SpreadsheetApp.getUi()
              .createMenu('User')
              .addItem('Create Form', 'createUsersForm')
              .addItem('Load Existing', 'loadExistingUsers'))
  .addSeparator()
  .addSubMenu(SpreadsheetApp.getUi()
              .createMenu('Asset / Device')
              .addItem('Create Form', 'ls_createForm')
              .addSeparator()
              .addSubMenu(SpreadsheetApp.getUi()
                         .createMenu('Lifesmart Device')
                         .addItem('Load Existing', 'ls_loadExistingDevices'))
              .addSeparator()
              .addSubMenu(SpreadsheetApp.getUi()
                         .createMenu('Asset')
                         .addItem('Load Existing', 'ls_loadExistingAssets'))
             )
  .addSeparator()
  .addSubMenu(SpreadsheetApp.getUi()
              .createMenu('Execute')
              .addItem('All Cells', 'executeAll')
              .addItem('Selected cell', 'executeSelected'))
  .addToUi();
}

function formIsFor(cols, rowIndex) {
  var sheet = SpreadsheetApp.getActiveSheet();
  
  var keys = Object.keys(cols).sort(function (k1, k2) { return cols[k1].idx < cols[k2].idx; });
  
  var values = sheet.getRange(cols[keys[0]].col + rowIndex + ':' + cols[keys[keys.length - 1]].col + rowIndex).getValues();
  return keys.filter(function (key) {
    return values[0][cols[key].idx] == cols[key].name;
  }).length == keys.length;
}

function executeAll() {
  if (formIsFor(USER_COL, 9)) {
    userExecuteAll();
  } else if (formIsFor(LS.COL, LS.START_ROW - 1)) {
    lifesmartDeviceExecuteAll();
  } else {
    var sheet = SpreadsheetApp.getActiveSheet();
    var progressRange = sheet.getRange('B1');
    progressRange.setValue('Form Format Error.');
    progressRange.activate();
  }
}

function executeSelected() {
  if (formIsFor(USER_COL, 9)) {
    userExecuteSelected();
  } else if (formIsFor(LS.COL, LS.START_ROW - 1)) {
    lifesmartDeviceExecuteSelected();
  } else {
    var sheet = SpreadsheetApp.getActiveSheet();
    var progressRange = sheet.getRange('B1');
    progressRange.setValue('Form Format Error.');
    progressRange.activate();
  }
}

function promiseRun (func) {
  
  // this is a trick to convert the arguments array into an array, and drop the first one
  var runArgs = Array.prototype.slice.call(arguments).slice(1);
  
  return new Promise (function (resolve, reject) {
    google.script.run
    .withSuccessHandler (function (result) {
      resolve (result);
    })
    .withFailureHandler (function (error) {
      reject (error);
    })
    [func].apply (this , runArgs) ;
    
  })
  
}