var LS = {
  COL: {
    STATUS: { idx: 0, col: 'A', name: 'Status' },
    ID: { idx: 1, col: 'B', name: 'ID (from Cereb)' },
    NAME_LS: { idx: 2, col: 'C', name: 'Name (from Lifesmart)' },
    NAME: { idx: 3, col: 'D', name: 'Name' },
    DESCRIPTION: { idx: 4, col: 'E', name: 'Description' },
    TAGS: { idx: 5, col: 'F', name: 'Tags' },
    ME: { idx: 6, col: 'G', name: 'Me' },
    AGT: { idx: 7, col: 'H', name: 'Agt' },
    DEVICE_TYPE: { idx: 8, col: 'I', name: 'Device Type' },
    ACTION: { idx: 9, col: 'J', name: 'Action' }
  },
  START_ROW: 10,
  DEVICE_MAPPING: [
    {
      device_type: 'SL_SC_BG',
      cereb: {
        name: 'lifesmart_cube_door_window_sensor',
        category: 'device',
        connector_type: 'lifesmart-cloud'
      }
    },
    {
      device_type: 'SL_SC_BM',
      cereb: {
        name: 'lifesmart_motion_sensor',
        category: 'device',
        connector_type: 'lifesmart-cloud'
      }
    },
    {
      device_type: 'SL_SC_BB',
      cereb: {
        name: 'lifesmart_cube_clicker',
        category: 'device',
        connector_type: 'lifesmart-cloud'
      }
    },
    {
      device_type: 'SL_LI_RGBW',
      cereb: {
        name: 'lifesmart_blend_light_bulb',
        category: 'device',
        connector_type: 'lifesmart-cloud'
      }
    },
    {
      device_type: 'SL_SC_WA',
      cereb: {
        name: 'lifesmart_pressure_sensor',
        category: 'device',
        connector_type: 'lifesmart-cloud'
      }
    }
  ],
  
  // Functions
  createForm: function() {
    var sheets = SpreadsheetApp.getActiveSpreadsheet().getSheets();
    var prefixName = 'Asset/Device List';
    var sheetName = prefixName;
    var index = 2;
    while (true) {
      if (sheets.filter(function (sheet) { return sheet.getName() == sheetName;}).length > 0) {
        sheetName = prefixName + ' - ' + index;
        index++;
      } else {
        break;
      }
    }
    var sheet = SpreadsheetApp.getActive().insertSheet(sheetName);
    
    sheet.getRange('A1:B7').setValues(
      [
        ['Progress', ''],
        ['', ''],
        ['Domain', ''],
        ['Organization Name', ''],
        ['Email', ''],
        ['Password', ''],
        ['Default Description', '']
      ]);
    sheet.getRange('A1:A7').setFontWeight('bold');
    
    sheet.getRange('D3:E6').setValues(
      [
        ['Lifesmart Acc', ''],
        ['Password', ''],
        ['Appkey', ''],
        ['Apptoken', '']
      ]);
    sheet.getRange('D3:D6').setFontWeight('bold');
    
    sheet.getRange(LS.COL.STATUS.col + (LS.START_ROW - 1) + ':' + LS.COL.ACTION.col + (LS.START_ROW - 1)).setValues(
      [
        [LS.COL.STATUS.name, LS.COL.ID.name, LS.COL.NAME_LS.name, LS.COL.NAME.name, LS.COL.DESCRIPTION.name,
         LS.COL.TAGS.name, LS.COL.ME.name, LS.COL.AGT.name, LS.COL.DEVICE_TYPE.name, LS.COL.ACTION.name]
      ]
    ).setNotes(
      [
        [
          '',
          '',
          '',
          '',
          'If empty, it will use Default Description.',
          '',
          '',
          '',
          '',
          ''
        ]
      ]
    ).setFontWeight('bold');
    
    sheet.setFrozenRows(9);
    
    sheet.getRange(LS.COL.ACTION.col + '10:' + LS.COL.ACTION.col + '10000').setDataValidation(SpreadsheetApp.newDataValidation().requireValueInList(['Create Asset', 'Find Id', 'Delete Asset'], true));
    
    sheet.autoResizeColumns(1, 9);
    sheet.setColumnWidth(LS.COL.NAME_LS.idx + 1, 280);
    sheet.setColumnWidth(LS.COL.NAME.idx + 1, 200);
    sheet.setColumnWidth(LS.COL.TAGS.idx + 1, 180);
    sheet.setColumnWidth(LS.COL.ME.idx + 1, 64);
    sheet.setColumnWidth(LS.COL.AGT.idx + 1, 180);
    sheet.setColumnWidth(LS.COL.ACTION.idx + 1, 100);
  },
  
  prepare: function(sheet, progressRange) {
    // Sign in
  
    sheet.getRange('D3:E6').activate();
    progressRange.setValue('Auth Lifesmart account - ' + sheet.getRange('E3').getValue() + '.');
    
    var values = sheet.getRange('E3:E6').getValues();
    lifesmartSignIn(values[0][0].toString(), values[1][0].toString(), values[2][0].toString(), function (response, errorCode, errorMsg) {
      if (errorCode) {
        progressRange.setValue('Auth Fail: ' + errorCode + ' - ' + errorMsg);
        return;
      }
      sequenceId = 0;
      lifesmartAuthUser = response;
    });
  },
  
  device: {
    loadExisting: function() {
      var sheet = SpreadsheetApp.getActiveSheet();
      var progressRange = sheet.getRange('B1');
      
      LS.prepare(sheet, progressRange);
      
      if (!lifesmartAuthUser) { return; }
      
      var appkey = sheet.getRange('E5').getValue();
      var apptoken = sheet.getRange('E6').getValue();
      
      sheet.getRange('G3:G6').setValues([
        [lifesmartAuthUser.expiredtime],
        [lifesmartAuthUser.svrurl],
        [lifesmartAuthUser.usertoken],
        [lifesmartAuthUser.userid]
      ]);
      
      progressRange.setValue('Getting Lifesmart Devices...');
      
      lifesmartEpGetAll(lifesmartAuthUser, appkey, apptoken, function (response, errorCode, errorMsg) {
        if (errorCode) {
          progressRange.setValue('Get Lifesmart Devices Fail: ' + errorCode + ' - ' + errorMsg);
          
          existingLifesmartDevices = nil;
          return;
        }
        
        existingLifesmartDevices = response.message;
      });
      
      if (!existingLifesmartDevices) {
        progressRange.setValue('Finish.');
        sheet.getRange(LS.COL.STATUS.col + LS.START_ROW + ':' + LS.COL.ACTION.col + (10000 + LS.START_ROW)).clear();
        return
      }
      
      progressRange.setValue('Cleaning...');
      sheet.getRange(LS.COL.STATUS.col + LS.START_ROW + ':' + LS.COL.ACTION.col + (10000 + LS.START_ROW)).clear();
      SpreadsheetApp.flush();
      Utilities.sleep(500);
      
      progressRange.setValue('Rendering...');
      SpreadsheetApp.flush();
      
      var index = LS.START_ROW;
      sheet.getRange(LS.COL.NAME_LS.col + index + ':' + LS.COL.ACTION.col + (index + existingLifesmartDevices.length - 1)).setValues(existingLifesmartDevices.map(function (device) {
        var names = device.name ? device.name.split('|') : [];
        var name = device.name ? names[names.length - 1] : '';
        var tags = device.name ? names.slice(0, names.length - 1) : [];
        return [
          device.name ? device.name : '',
          names.join(' '),
          '',
          tags.join('|'),
          device.me,
          device.agt,
          device.devtype,
          ''
        ];
      }));
      
      progressRange.setValue('Finish.');
      progressRange.activate();
    }
  },
  
  asset: {
    loadExisting: function() {
      var sheet = SpreadsheetApp.getActiveSheet();
      var progressRange = sheet.getRange('B1');
      
      domain = sheet.getRange('B3').getValue();
      
      // Sign in
      
      sheet.getRange('A3:B7').activate();
      progressRange.setValue('Verifying account - ' + sheet.getRange('B4').getValue() + '.');
      
      var values = sheet.getRange('B3:B6').getValues();
      
      signIn(values[1][0], values[2][0], values[3][0], function (response, errorCode, errorMsg) {
        if (errorCode) {
          progressRange.setValue('Verify Fail (' + errorCode + ') - ' + errorMsg);
          user = null;
        } else {
          progressRange.setValue('Verify Sucessfully.');
          user = response.result[0];
        }
      });
      
      if (!user) { return; }
      
      Utilities.sleep(1000);
      
      // Get Existing Asset Templates
      progressRange.setValue('Getting Asset Templates...');
      var assetTemplates = null;
      cerebGetAssetTemplates(user.access_token, function (response, errorCode, errorMsg) {
        if (errorCode) {
          progressRange.setValue('Get Asset Templates Fail (' + errorCode + ') - ' + errorMsg);
        } else if (response.result && response.result.length > 0) {
          progressRange.setValue('Get Asset Templates Sucessfully.');
          assetTemplates = response.result;
        } else {
          progressRange.setValue('Empty Asset Templates.');
        }
      });
      
      if (!assetTemplates) return;
      
      // Update asset template's id to mapping objects
      
      LS.DEVICE_MAPPING.forEach(function(mapping) {
        var filteredAssetTemplates = assetTemplates.filter(function(template) {
          return template.name == mapping.cereb.name && template.category == mapping.cereb.category && template.connector_type == mapping.cereb.connector_type;
        });
        if (filteredAssetTemplates && filteredAssetTemplates.length > 0) {
          mapping.asset_template_id = filteredAssetTemplates[0].id;
        }
      });
      
      // Get Existing Assets
      progressRange.setValue('Getting Assets...');
      var assets = [];
      var page = 1;
      var loop = true;
      while (loop) {
        cerebGetAssets(user.access_token, page, function (response, errorCode, errorMsg) {
          if (errorCode) {
            progressRange.setValue('<' + page + '>Get Assets Fail (' + errorCode + ') - ' + errorMsg);
            assets = null;
            loop = false;
          } else {
            assets = assets.concat(response.result);
            progressRange.setValue('Getting Assets(' + (page + 1) + '/' + response.pagination.total_pages + ')...');
          }
          
          if (page == response.pagination.total_pages) {
            loop = false;
          }
          
          page++;
        });
      }
      
      if (assets == null) return;
      
      progressRange.setValue('Cleaning...');
      sheet.getRange(LS.COL.STATUS.col + LS.START_ROW + ':' + LS.COL.ACTION.col + (10000 + LS.START_ROW)).clear();
      SpreadsheetApp.flush();
      Utilities.sleep(500);
      
      sheet.getRange(LS.COL.ID.col + LS.START_ROW + ':' + LS.COL.DEVICE_TYPE.col + (LS.START_ROW + assets.length - 1)).setValues(assets.map(function (asset) {
        var filteredMappings = LS.DEVICE_MAPPING.filter(function(mapping) {
          return asset.template.id == mapping.asset_template_id;
        });
        if (filteredMappings && filteredMappings.length > 0) {
          return [
            asset.id,
            '',
            asset.display_name,
            asset.desc,
            asset.tags.join('|'),
            asset.create_required.lifesmart_device_id,
            asset.create_required.agt,
            filteredMappings[0].device_type
          ];
        } else {
          return [
            asset.id,
            '',
            asset.display_name,
            asset.desc,
            asset.tags.join('|'),
            '',
            '',
            ''
          ];
        }
      }));
      
      progressRange.setValue('Finish.');
    }
  }
};

var did = Utilities.getUuid();
var lifesmartAuthUser = {};
var sequenceId = 0;

var existingLifesmartDevices = [];

function ls_createForm() {
  LS.createForm();
}

function ls_loadExistingDevices() {
  LS.device.loadExisting();
}

function ls_loadExistingAssets() {
  LS.asset.loadExisting();
}

function lifesmartPrepareForExecute(sheet, progressRange) {
  // Sign in
  
  sheet.getRange('A3:B7').activate();
  progressRange.setValue('Verifying account - ' + sheet.getRange('B4').getValue() + '.');
  
  var values = sheet.getRange('B3:B6').getValues();
  
  signIn(values[1][0], values[2][0], values[3][0], function (response, errorCode, errorMsg) {
    if (errorCode) {
      progressRange.setValue('Verify Fail (' + errorCode + ') - ' + errorMsg);
      user = null;
    } else {
      progressRange.setValue('Verify Sucessfully.');
      user = response.result[0];
    }
  });
  
  if (!user) { return; }
  
  Utilities.sleep(1000);
  
  // Get Existing Asset Templates
  progressRange.setValue('Getting Asset Templates...');
  var assetTemplates = null;
  cerebGetAssetTemplates(user.access_token, function (response, errorCode, errorMsg) {
    if (errorCode) {
      progressRange.setValue('Get Asset Templates Fail (' + errorCode + ') - ' + errorMsg);
    } else if (response.result && response.result.length > 0) {
      progressRange.setValue('Get Asset Templates Sucessfully.');
      assetTemplates = response.result;
    } else {
      progressRange.setValue('Empty Asset Templates.');
    }
  });
  
  if (!assetTemplates) return;
  
  // Get Existing Assets
  progressRange.setValue('Getting Assets...');
  var assets = [];
  var page = 1;
  var loop = true;
  while (loop) {
    cerebGetAssets(user.access_token, page, function (response, errorCode, errorMsg) {
      if (errorCode) {
        progressRange.setValue('<' + page + '>Get Assets Fail (' + errorCode + ') - ' + errorMsg);
        assets = null;
        loop = false;
      } else {
        assets = assets.concat(response.result);
      }
      
      if (page == response.pagination.total_pages) {
        loop = false;
      }
      
      page++;
    });
  }
  
  if (assets == null) return;
  
  progressRange.setValue('Starting...');
  
  // Update asset template's id to mapping objects
  
  LS.DEVICE_MAPPING.forEach(function(mapping) {
    var filteredAssetTemplates = assetTemplates.filter(function(template) {
      return template.name == mapping.cereb.name && template.category == mapping.cereb.category && template.connector_type == mapping.cereb.connector_type;
    });
    if (filteredAssetTemplates && filteredAssetTemplates.length > 0) {
      mapping.asset_template_id = filteredAssetTemplates[0].id;
    }
  });
  
  return {
    assets: assets,
    assetTemplates: assetTemplates
  };
}

function lifesmartDeviceExecuteAll() {
  var sheet = SpreadsheetApp.getActiveSheet();
  var progressRange = sheet.getRange('B1');
  
  domain = sheet.getRange('B3').getValue();
  
  var result = lifesmartPrepareForExecute(sheet, progressRange);
  
  if (!result) { return; }
  
  // Generate
  
  var index = LS.START_ROW;
  while (true) {
    var range = sheet.getRange(LS.COL.STATUS.col + index + ':' + LS.COL.ACTION.col + index);
    range.activate();
    
    var values = range.getValues();
    var filteredValues = values[0].filter(function(value) {
      return value == undefined || value == null || value.toString().length == 0;
    });
    if (filteredValues.length == values[0].length) {
      break;
    }
    
    var asset = createAssetByRangeValues(values);
    if (!asset.desc) {
      asset.desc = sheet.getRange('B7').getValue().toString();
    }
    
    var action = sheet.getRange(LS.COL.ACTION.col + index).getValue().toString();
    
    switch (action) {
      case 'Create Asset':
        var createdAsset = lifesmartDeviceCreateAsset(sheet, index, asset, values[0][LS.COL.DEVICE_TYPE.idx].toString());
        if (createdAsset) {
          result.assets.push(createdAsset);
        }
        break;
      case 'Find Id':
        lifesmartDeviceFindId(sheet, index, asset, result.assets);
        break;
      case 'Delete Asset':
        var deletedAsset = lifesmartDeviceDeleteAsset(sheet, index, asset);
        if (deletedAsset) {
          var index = result.assets.findIndex(deletedAsset);
          result.assets.splice(index, 1);
        }
        break;
    }
    
    index++;
  }
  
  progressRange.setValue('Finish');
  progressRange.activate();
  SpreadsheetApp.flush();
}

function lifesmartDeviceExecuteSelected() {
  var sheet = SpreadsheetApp.getActiveSheet();
  var progressRange = sheet.getRange('B1');
  
  var index = sheet.getActiveRange().getRow();
  if (index < LS.START_ROW) {
    progressRange.setValue('Error: Please select in user list.');
    progressRange.activate();
    return;
  }
  
  domain = sheet.getRange('B3').getValue();
  
  var result = lifesmartPrepareForExecute(sheet, progressRange);
  
  if (!result) { return; }
  
  // Generate
  
  var range = sheet.getRange(LS.COL.STATUS.col + index + ':' + LS.COL.ACTION.col + index);
  range.activate();
  
  var values = range.getValues();
  var filteredValues = values[0].filter(function(value) {
    return value == undefined || value == null || value.toString().length == 0;
  });
  if (filteredValues.length != values[0].length) {
    var asset = createAssetByRangeValues(values);
    if (!asset.desc) {
      asset.desc = sheet.getRange('B7').getValue().toString();
    }
    
    var action = sheet.getRange(LS.COL.ACTION.col + index).getValue().toString();
    
    switch (action) {
      case 'Create Asset':
        var createdAsset = lifesmartDeviceCreateAsset(sheet, index, asset, values[0][LS.COL.DEVICE_TYPE.idx].toString());
        if (createdAsset) {
          result.assets.push(createdAsset);
        }
        break;
      case 'Find Id':
        lifesmartDeviceFindId(sheet, index, asset, result.assets);
        break;
      case 'Delete Asset':
        var deletedAsset = lifesmartDeviceDeleteAsset(sheet, index, asset);
        if (deletedAsset) {
          var index = result.assets.findIndex(deletedAsset);
          result.assets.splice(index, 1);
        }
        break;
    }
  }
  
  progressRange.setValue('Finish');
  progressRange.activate();
  SpreadsheetApp.flush();
  
}

function lifesmartDeviceCreateAsset(sheet, index, asset, device_type) {
  if (!asset.display_name && !asset.description && !asset.create_required.lifesmart_device_id && !asset.create_required.agt) {
    sheet.getRange(LS.COL.STATUS.col + index).setValue('Required Field(s) is/are missing.');
  } else {
    sheet.getRange(LS.COL.STATUS.col + index).setValue('Processing...');
    
    var filteredMappings = LS.DEVICE_MAPPING.filter(function(mapping) {
      return mapping.device_type == device_type;
    });
    
    if (filteredMappings && filteredMappings.length > 0) {
      asset.asset_template_id = filteredMappings[0].asset_template_id;
      asset.destination = user.destination;
      
      var connectors = null;
      cerebGetConnectorsByAssetTemplate(user.access_token, asset.asset_template_id, function(response, errorCode, errorMsg) {
        if (errorCode) {
          range[0][LS.COL.STATUS.idx].setValue('Get Connectors Fail (' + errorCode + ') - ' + errorMsg);
        } else {
          connectors = response.result;
        }
      });
      
      if (connectors != null) {
        var lsAccount = sheet.getRange('E3:E4').getValues();
        var filteredConnectors = connectors.filter(function(connector) {
          return connector.create_required.email == lsAccount[0][0].toString() && connector.create_required.password == lsAccount[1][0].toString();
        });
        
        if (filteredConnectors && filteredConnectors.length > 0) {
          // Have existing connector
          asset.connector_id = filteredConnectors[0].id;
        } else {
          // Create connector
          cerebCreateConnector(user.access_token, 'Lifesmart - ' + lsAccount[0][0].toString(), lsAccount[0][0].toString(), asset.asset_template_id, {
            email: lsAccount[0][0].toString(), password: lsAccount[1][0].toString()
          }, asset.destination, function(response, errorCode, errorMsg) {
            if (errorCode) {
              sheet.getRange(LS.COL.STATUS.col + index).setValue('Create Connector Fail (' + errorCode + ') - ' + errorMsg);
            } else {
              asset.connector_id = response.result[0].id;
            }
          });
        }
        
        if (asset.connector_id) {
          // Create Asset
          cerebCreateAsset(user.access_token, asset, function(response, errorCode, errorMsg) {
            if (errorCode) {
              switch (errorCode) {
                case 'ERR30000':
                  sheet.getRange(LS.COL.STATUS.col + index).setValue('Already Created');
                  break;
                default:
                  sheet.getRange(LS.COL.STATUS.col + index).setValue('Create Asset Fail (' + errorCode + ') - ' + errorMsg);
                  break;
              }
            } else {
              sheet.getRange(LS.COL.STATUS.col + index).setValue('Create Successfully.');
              return response.result[0];
            }
          });
        }
      }
    } else {
      sheet.getRange(LS.COL.STATUS.col + index).setValue('Not support in Cereb.');
    }
  }
}

function lifesmartDeviceFindId(sheet, index, asset, assets) {
  if (asset.id) {
    sheet.getRange(LS.COL.STATUS.col + index).setValue('Already Have Id.');
  } else if (asset.create_required && asset.create_required.lifesmart_device_id, asset.create_required.agt) {
    sheet.getRange(LS.COL.STATUS.col + index).setValue('Processing...');
    
    var filteredAssets = assets.filter(function(_asset) {
      return _asset.create_required && _asset.create_required.lifesmart_device_id == asset.create_required.lifesmart_device_id && _asset.create_required.agt == asset.create_required.agt;
    });
    if (filteredAssets && filteredAssets.length > 0) {
      sheet.getRange(LS.COL.ID.col + index).setValue(filteredAssets[0].id);
      sheet.getRange(LS.COL.STATUS.col + index).setValue('Found it.');
    } else {
      sheet.getRange(LS.COL.STATUS.col + index).setValue('Not Found.');
    }
  } else {
    sheet.getRange(LS.COL.STATUS.col + index).setValue('Required Field(s) is/are missing.');
  }
}

function lifesmartDeviceDeleteAsset(sheet, index, asset) {
  var id = asset.id;
  if (!id) {
    sheet.getRange(LS.COL.STATUS.col + index).setValue('Required Field(s) is/are missing.');
  } else {
    sheet.getRange(LS.COL.STATUS.col + index).setValue('Processing...');
    
    cerebDeleteAsset(user.access_token, id, function(response, errorCode, errorMsg) {
      if (errorCode) {
        sheet.getRange(LS.COL.STATUS.col + index).setValue('Delete Asset Fail (' + errorCode + ') - ' + errorMsg);
      } else {
        sheet.getRange(LS.COL.STATUS.col + index).setValue('Deleted.');
        return asset;
      }
    });
  }
}

// APIs Call with callback

function lifesmartResultWithCallback(result, callback) {
  var responseJson = JSON.parse(result.getContentText());
  if (result.getResponseCode() == 200) {
    if (responseJson.status) {
      callback(responseJson, responseJson.status, responseJson.message);
    } else {
      callback(responseJson, responseJson.code == 'success' ? null : responseJson.code, responseJson.message);
    }
  } else {
    callback(responseJson, result.getResponseCode(), null);
  }
}

// APIs Call

function lifesmartSignIn(uid, pwd, appkey, callback) {
  var params = {
    'uid': uid,
    'pwd': pwd,
    'appkey': appkey,
    'did': did
  };
  
  var options = {
    'method': 'post',
    'payload': params
  };
  
  var result = UrlFetchApp.fetch('https://api.ilifesmart.com/app/auth.login', options);
  lifesmartResultWithCallback(result, function (response, errorCode, errorMsg) {
    if (errorCode) {
      callback(response, errorCode, errorMsg);
      return;
    }
    
    lifesmartAuth(response.userid, appkey, response.token, did, response.rgn, callback);
  });
}

function lifesmartAuth(userid, appkey, token, did, rgn, callback) {
  var params = {
    'userid': userid,
    'appkey': appkey,
    'token': token,
    'did': did,
    'rgn': rgn,
  };
  
  var options = {
    'method': 'post',
    'payload': params
  };
  
  var result = UrlFetchApp.fetch('https://api.ilifesmart.com/app/auth.do_auth', options);
  if (callback) {
    lifesmartResultWithCallback(result, callback);
  } else {
    return result;
  }
}

function lifesmartEpGetAll(authUser, appkey, apptoken, callback) {
  var ts = parseInt((new Date()).getTime() / 1000);
  var params = {
    'id': sequenceId,
    'method': 'EpGetAll',
    'system': {
      'ver': '1.0',
      'lang': 'en',
      'did': did,
      'userid': authUser.userid,
      'appkey': appkey,
      'time': ts,
      'sign': MD5([
        ['method', 'EpGetAll'].join(':'),
        ['did', did].join(':'),
        ['time', ts].join(':'),
        ['userid', authUser.userid].join(':'),
        ['usertoken', authUser.usertoken].join(':'),
        ['appkey', appkey].join(':'),
        ['apptoken', apptoken].join(':')
      ].join(',')).toLowerCase()
    }
  }
  
  var options = {
    'method': 'post',
    'muteHttpExceptions': true,
    'payload': JSON.stringify(params),
    'contentType': 'application/json'
  };
  
  var result = UrlFetchApp.fetch(authUser.svrurl + '/api.EpGetAll', options);
  
  sequenceId++;
  
  if (callback) {
    lifesmartResultWithCallback(result, callback);
  } else {
    return result;
  }
}

function cerebGetAssetTemplates(access_token, callback) {
  var options = {
    'method': 'get',
    'muteHttpExceptions': true,
    'headers': {
      'X-AUTH-TOKEN': access_token
    },
    'contentType': 'application/json'
  };
  
  var result = UrlFetchApp.fetch(domain + '/asset_template', options);
  if (callback) {
    resultWithCallback(result, callback);
  } else {
    return result;
  }
}

function cerebGetAssets(access_token, page, callback) {
  var options = {
    'method': 'get',
    'muteHttpExceptions': true,
    'headers': {
      'X-AUTH-TOKEN': access_token
    },
    'contentType': 'application/json'
  };
  
  var result = UrlFetchApp.fetch(domain + '/asset?sort=desc&limit=100&page=' + page, options);
  if (callback) {
    resultWithCallback(result, callback);
  } else {
    return result;
  }
}

function cerebGetConnectorsByAssetTemplate(access_token, asset_template_id, callback) {
  var options = {
    'method': 'get',
    'muteHttpExceptions': true,
    'headers': {
      'X-AUTH-TOKEN': access_token
    },
    'contentType': 'application/json'
  };
  
  var result = UrlFetchApp.fetch(domain + '/connector/asset_template/' + asset_template_id, options);
  if (callback) {
    resultWithCallback(result, callback);
  } else {
    return result;
  }
}

function cerebCreateConnector(access_token, display_name, description, asset_template_id, create_required, destination, callback) {
  var options = {
    'method': 'post',
    'muteHttpExceptions': true,
    'payload': JSON.stringify({
      'display_name': display_name,
      'desc': description,
      'asset_template_id': asset_template_id,
      'create_required': create_required,
      'destination': destination
    }),
    'headers': {
      'X-AUTH-TOKEN': access_token
    },
    'contentType': 'application/json'
  };
  
  var result = UrlFetchApp.fetch(domain + '/connector', options);
  if (callback) {
    resultWithCallback(result, callback);
  } else {
    return result;
  }
}

function cerebCreateAsset(access_token, asset, callback) {
  var options = {
    'method': 'post',
    'muteHttpExceptions': true,
    'payload': JSON.stringify(asset),
    'headers': {
      'X-AUTH-TOKEN': access_token
    },
    'contentType': 'application/json'
  };
  
  var result = UrlFetchApp.fetch(domain + '/asset', options);
  if (callback) {
    resultWithCallback(result, callback);
  } else {
    return result;
  }
}

function cerebDeleteAsset(access_token, asset_id, callback) {
  var options = {
    'method': 'delete',
    'muteHttpExceptions': true,
    'headers': {
      'X-AUTH-TOKEN': access_token
    },
    'contentType': 'application/json'
  };
  
  var result = UrlFetchApp.fetch(domain + '/asset/' + asset_id, options);
  if (callback) {
    resultWithCallback(result, callback);
  } else {
    return result;
  }
}

function createAssetByRangeValues(values) {
  return {
    id: values[0][LS.COL.ID.idx].toString(),
    name: values[0][LS.COL.NAME.idx].toString().replace(/([ -\/|])/g, "_").toLowerCase(),
    display_name: values[0][LS.COL.NAME.idx].toString(),
    desc: values[0][LS.COL.DESCRIPTION.idx].toString(),
    tags: values[0][LS.COL.TAGS.idx].toString().length > 0 ? values[0][LS.COL.TAGS.idx].toString().split('|') : [],
    images: {
      thumbnail: '',
      large: ''
    },
    asset_template_id: '',
    connector_id: '',
    create_required: {
      lifesmart_device_id: values[0][LS.COL.ME.idx].toString(),
      agt: values[0][LS.COL.AGT.idx].toString()
    },
    is_native: true
  };
}