var domain = '';
var user = {};
var existingUsers = [];
var resourceTypes = [];

var USER_COL = {
  STATUS: { idx: 0, col: 'A', name: 'Status' },
  FIRST_NAME: { idx: 1, col: 'B', name: 'First name' },
  LAST_NAME: { idx: 2, col: 'C', name: 'Last name' },
  USERNAME: { idx: 3, col: 'D', name: 'Username' },
  EMAIL: { idx: 4, col: 'E', name: 'Email' },
  PASSWORD: { idx: 5, col: 'F', name: 'Password' },
  TELEGRAM_ID: { idx: 6, col: 'G', name: 'Telegram Id' },
  MOBILE_DING_TALK: { idx: 7, col: 'H', name: 'Mobile (Ding Talk)' },
  ROLE: { idx: 8, col: 'I', name: 'Role' },
  ACTION: { idx: 9, col: 'J', name: 'Action' }
};

var RES_TYPE = {
  TELEGRAM_CHAT_ID: {
    type: 0
  },
  DINGTALK_PHONE_NUMBER: {
    type: 1
  },
  DINGTALK_USERNAME: {
    type: 2
  }
}

function createUsersForm() {
  var sheets = SpreadsheetApp.getActiveSpreadsheet().getSheets();
  var prefixName = 'User List';
  var sheetName = prefixName;
  var index = 2;
  while (true) {
    if (sheets.filter(function (sheet) { return sheet.getName() == sheetName;}).length > 0) {
      sheetName = prefixName + ' - ' + index;
      index++;
    } else {
      break;
    }
  }
  var sheet = SpreadsheetApp.getActive().insertSheet(sheetName);
  
  sheet.getRange('A1:B7').setValues(
    [
      ['Progress', ''],
      ['', ''],
      ['Domain', ''],
      ['Organization Name', ''],
      ['Email', ''],
      ['Password', ''],
      ['Username Prefix', '']
    ]);
  sheet.getRange('A1:A7').setFontWeight('bold');
  
  sheet.getRange(USER_COL.STATUS.col + '9:' + USER_COL.ACTION.col + '9').setValues(
    [
      [USER_COL.STATUS.name, USER_COL.FIRST_NAME.name, USER_COL.LAST_NAME.name, USER_COL.USERNAME.name, USER_COL.EMAIL.name, 
       USER_COL.PASSWORD.name, USER_COL.TELEGRAM_ID.name, USER_COL.MOBILE_DING_TALK.name, USER_COL.ROLE.name, USER_COL.ACTION.name]
    ]
  ).setNotes(
    [
      [
        '',
        '',
        '',
        '=CONCAT("@", REGEXREPLACE(JOIN("", $B$7, LOWER(B10), LOWER(C10)), "[^a-zA-Z0-9.]", ""))',
        '=CONCAT(REGEXREPLACE(JOIN(".", LOWER(B10), LOWER(C10)),"[^a-zA-Z0-9.]", ""), "@testing.com")',
        '',
        '',
        '',
        'Default Role: Staff',
        ''
      ]
    ]
  ).setFontWeight('bold');
  
  sheet.setFrozenRows(9);
  
  sheet.getRange(USER_COL.ROLE.col + '10:' + USER_COL.ROLE.col + '10000').setDataValidation(SpreadsheetApp.newDataValidation().requireValueInList(['Admin', 'Staff'], true));
  sheet.getRange(USER_COL.ACTION.col + '10:' + USER_COL.ACTION.col + '10000').setDataValidation(SpreadsheetApp.newDataValidation().requireValueInList(['Create', 'Update'], true));
  
  sheet.autoResizeColumns(1, 9);
  sheet.setColumnWidth(USER_COL.FIRST_NAME.idx + 1, 280);
  sheet.setColumnWidth(USER_COL.EMAIL.idx + 1, 180);
  sheet.setColumnWidth(USER_COL.ROLE.idx + 1, 64);
  sheet.setColumnWidth(USER_COL.ACTION.idx + 1, 180);
}

function prepare(sheet, progressRange) {
  domain = sheet.getRange('B3').getValue();
  
  // Sign in
  
  sheet.getRange('A3:B7').activate();
  progressRange.setValue('Verifying account - ' + sheet.getRange('B4').getValue() + '.');
  
  var values = sheet.getRange('B3:B6').getValues();
  
  signIn(values[1][0], values[2][0], values[3][0], function (response, errorCode, errorMsg) {
    if (errorCode) {
      progressRange.setValue('Verify Fail (' + errorCode + ') - ' + errorMsg);
      user = null;
    } else {
      progressRange.setValue('Verify Sucessfully.');
      user = response.result[0];
    }
  });
  
  if (!user) { return; }
  
  progressRange.setValue('Starting...');
  Utilities.sleep(1000);
  
  // Get Existing Users
  
  getOrganizationUsers(user.access_token, function (response, errorCode, errorMsg) {
    if (errorCode) {
      progressRange.setValue('Get Existing Users Fail (' + responseJson.status_code + ') - ' + responseJson.msg);
      existingUsers = null;
    } else {
      progressRange.setValue('Got Existing Users.');
      existingUsers = response.result;
      
//      if (existingUsers) {
//        existingUsers.forEach(function(_user) {
//          getUserProfile(user.access_token, _user.id, function(response, errorCode, errorMsg) {
//            if (errorCode) {
//            } else if (response.result && response.result.length > 0) {
//              var __user = response.result[0];
//              var idx = existingUsers.map(function(_user) {return _user.id;}).indexOf(__user.id);
//              if (idx >= 0) {
//                existingUsers[idx] = __user;
//              }
//            }
//          });
//        });
//      }
    }
  });
  
  // Get Resource Types
  
  getResourceTypes(user.access_token, function(response, errorCode, errorMsg) {
    if (errorCode) {
      progressRange.setValue('Get Resource Types Fail (' + responseJson.status_code + ') - ' + responseJson.msg);
      resourceTypes = null;
    } else {
      progressRange.setValue('Got Resource Types.');
      resourceTypes = response.result;
    }
  });
}

function loadExistingUsers() {
  var sheet = SpreadsheetApp.getActiveSheet();
  var progressRange = sheet.getRange('B1');
  
  prepare(sheet, progressRange);
  
  if (!existingUsers) {
    progressRange.setValue('Finish.');
    sheet.getRange(USER_COL.STATUS.col + '10:' + USER_COL.ACTION.col + '10000').clear();
    return;
  }
  
  progressRange.setValue('Cleaning...');
  sheet.getRange(USER_COL.STATUS.col + '10:' + USER_COL.ACTION.col + '10000').clear();
  SpreadsheetApp.flush();
  Utilities.sleep(500);
  
  progressRange.setValue('Rendering...');
  SpreadsheetApp.flush();
  
  var index = 10;
  sheet.getRange(USER_COL.FIRST_NAME.col + index + ':' + USER_COL.ROLE.col + (index + existingUsers.length - 1)).setValues(existingUsers.map(function (user) {
    var adminRoles = [
      getRoleId('ORGANIZATION_ACCOUNT_USER'),
      getRoleId('ORGANIZATION_ASSET_CREATOR'),
      getRoleId('ORGANIZATION_ASSET_TEMPLATE_CREATOR'),
      getRoleId('ORGANIZATION_ASSET_TEMPLATE_USER'),
      getRoleId('ORGANIZATION_ASSET_TEMPLATE_VIEWER'),
      getRoleId('ORGANIZATION_ASSET_USER'),
      getRoleId('ORGANIZATION_ASSET_VIEWER'),
      getRoleId('ORGANIZATION_CONTRACT_ADMIN'),
      getRoleId('ORGANIZATION_CONTRACT_TEMPLATE_CREATOR'),
      getRoleId('ORGANIZATION_CONTRACT_TEMPLATE_USER'),
      getRoleId('ORGANIZATION_CONTRACT_TEMPLATE_VIEWER'),
      getRoleId('ORGANIZATION_CONTRACT_USER'),
      getRoleId('ORGANIZATION_PROBE_TYPE_USER'),
      getRoleId('ORGANIZATION_PROBE_TYPE_VIEWER'),
      getRoleId('ORGANIZATION_USER_ADMIN'),
      getRoleId('ORGANIZATION_USER_USER'),
      getRoleId('SYSTEM_ASSET_TEMPLATE_USER'),
      getRoleId('SYSTEM_ASSET_TEMPLATE_VIEWER'),
      getRoleId('SYSTEM_CONTRACT_TEMPLATE_USER'),
      getRoleId('SYSTEM_CONTRACT_TEMPLATE_VIEWER'),
      getRoleId('SYSTEM_PROBE_TYPE_USER'),
      getRoleId('SYSTEM_PROBE_TYPE_VIEWER')
    ];
    
    var staffRoles = [
      getRoleId('ORGANIZATION_ACCOUNT_USER'),
      getRoleId('ORGANIZATION_USER_USER')
    ];
    
    var role = '';
    
    var roleIds = [];
    if (user.roles.role) {
      roleIds = user.roles.role.map(function (role) { return role.id; });
    } else {
      roleIds = user.roles.map(function (role) { return role.id; });
    }
    
    SpreadsheetApp.flush();
    
    if (roleIds.length == staffRoles.length) {
      role = staffRoles.filter(function (role) { return roleIds.indexOf(role) != -1; }).length == staffRoles.length ? 'Staff' : '';
    } else {
      role = adminRoles.filter(function (role) { return roleIds.indexOf(role) != -1; }).length == adminRoles.length ? 'Admin' : '';
    }
    
    var telegramId = '';
    var dingTalkNum = '';
    
    if (user.information) {
      var filteredInfos = user.information.filter(function(info) {
        return info.type == RES_TYPE.TELEGRAM_CHAT_ID.type;
      });
      telegramId = filteredInfos.length > 0 ? filteredInfos[0].value : '';
      
      filteredInfos = user.information.filter(function(info) {
        return info.type == RES_TYPE.DINGTALK_PHONE_NUMBER.type;
      });
      dingTalkNum = filteredInfos.length > 0 ? filteredInfos[0].value : '';
    }
    
    return [user.first_name, user.last_name, user.username, user.email, '', telegramId, dingTalkNum, role];
  }));
  
  progressRange.setValue('Finish.');
  progressRange.activate();
}

function userExecuteAll() {
  var sheet = SpreadsheetApp.getActiveSheet();
  var progressRange = sheet.getRange('B1');
  
  prepare(sheet, progressRange);
  
  if (!existingUsers) { return; }
  Utilities.sleep(500);
  
  var indexesForEdit = [];
  var indexesForUpdateTelegramId = [];
  var indexesForUpdateDingTalkMobile = [];
  
  // Create User
  
  var index = 10
  
  if (true) {
    progressRange.setValue('Filtering...');
    var values = sheet.getRange(USER_COL.STATUS.col + index + ':' + USER_COL.ACTION.col + sheet.getLastRow()).getValues().map(function(value, idx) {
      return {
        index: index + idx,
        user: createUserByRangeValues([value]),
        action: value[USER_COL.ACTION.idx].toString()
      };
    }).filter(function(o) {
      var _user = o.user;
      if (!_user.first_name && !_user.last_name && !_user.username && !_user.email && !_user.password) {
        return false;
      } else {
        return true;
      }
    });
    
    progressRange.setValue('Filtered.');
    var valuesForUpdate = [];
    
    values.forEach(function(v) {
      var range = sheet.getRange(USER_COL.STATUS.col + v.index + ':' + USER_COL.ACTION.col + v.index);
      range.activate();
      SpreadsheetApp.flush();
      
      switch (v.action) {
        case 'Create':
          createUserWithIndex(sheet, v.index, v.user);
          break;
        case 'Update':
          valuesForUpdate.push(v);
          sheet.getRange(USER_COL.STATUS.col + v.index).setValue('Waiting...');
          break;
      }
      SpreadsheetApp.flush();
      Utilities.sleep(50);
    });
    
    valuesForUpdate.forEach(function(v) {
      updateUserInfoWithIndex(sheet, v.index, v.user);
      Utilities.sleep(50);
    });
  } else {
    while (true) {
      var range = sheet.getRange(USER_COL.STATUS.col + index + ':' + USER_COL.ACTION.col + index);
      range.activate();
      
      var values = range.getValues();
      var _user = createUserByRangeValues(values);
      
      if (!_user.first_name && !_user.last_name && !_user.username && !_user.email && !_user.password) {
        break;
      }
      
      var action = values[0][USER_COL.ACTION.idx].toString();
      
      switch (action) {
        case 'Create':
          createUserWithIndex(sheet, index);
          break;
        case 'Update':
          indexesForEdit.push(index);
          sheet.getRange(USER_COL.STATUS.col + index).setValue('Waiting...')
          break;
        case 'Update Telegram Id':
          indexesForUpdateTelegramId.push(index);
          sheet.getRange(USER_COL.STATUS.col + index).setValue('Waiting...')
          break;
        case 'Update Mobile (Ding Talk)':
          indexesForUpdateDingTalkMobile.push(index);
          sheet.getRange(USER_COL.STATUS.col + index).setValue('Waiting...')
          break;
      }
      
      Utilities.sleep(100);
      index++;
    }
  }
  
  // Edit Users
  
  indexesForEdit.forEach(function (index) {
    updateUserInfoWithIndex(sheet, index);
  });
  
  // Update User's telegram id
  
  indexesForUpdateTelegramId.forEach(function (index) {
    updateUserTelegramIdWithIndex(sheet, index);
  });
  
  // Update User's DingTalk Mobile
  
  indexesForUpdateDingTalkMobile.forEach(function (index) {
    updateUserDingTalkMobileWithIndex(sheet, index);
  });
  
  progressRange.setValue('Finish.');
  progressRange.activate();
}

function userExecuteSelected() {
  var sheet = SpreadsheetApp.getActiveSheet();
  var progressRange = sheet.getRange('B1');
  
  var index = sheet.getActiveRange().getRow();
  if (index < 10) {
    progressRange.setValue('Error: Please select in user list.');
    progressRange.activate();
    return;
  }
  
  prepare(sheet, progressRange);
  
  var range = sheet.getRange(USER_COL.STATUS.col + index + ':' + USER_COL.ACTION.col + index);
  range.activate();
  
  var values = range.getValues();
  var _user = createUserByRangeValues(values);
  
  if (!_user.first_name && !_user.last_name && !_user.username && !_user.email && !_user.password) {
    progressRange.setValue('Error: Please select in user list.');
    progressRange.activate();
    return;
  }
  
  var action = values[0][USER_COL.ACTION.idx].toString();
  
  switch (action) {
    case 'Create':
      createUserWithIndex(sheet, index);
      break;
    case 'Update':
      updateUserInfoWithIndex(sheet, index);
      break;
    case 'Update Telegram Id':
      updateUserTelegramIdWithIndex(sheet, index);
      break;
    case 'Update Mobile (Ding Talk)':
      updateUserDingTalkMobileWithIndex(sheet, index);
      break;
  }
  
  progressRange.setValue('Finish.');
  progressRange.activate();
}

function createUserWithIndex(sheet, index, __user) {
  var range = sheet.getRange(USER_COL.STATUS.col + index + ':' + USER_COL.ACTION.col + index);
  range.activate();
  
  var _user = {};
  if (__user) {
    _user = __user;
  } else {
    var values = range.getValues();
    _user = createUserByRangeValues(values);
  }
  
  var missingFields = [];
  if (!_user.first_name) missingFields.push('First Name');
  if (!_user.last_name) missingFields.push('Last Name');
  if (!_user.username) missingFields.push('Username');
  if (!_user.email) missingFields.push('Email');
  if (!_user.password) missingFields.push('Password');
  
  if (missingFields.length > 0) {
    sheet.getRange(USER_COL.STATUS.col + index).setValue('Missing Fields: ' + missingFields.join(', '));
  } else {
    sheet.getRange(USER_COL.STATUS.col + index).setValue('Processing...');
    
    createOrganizationUser(user.access_token, JSON.stringify(_user), function (response, errorCode, errorMsg) {
      if (errorCode) {
        switch (errorCode) {
          case 'ERR00005':
            sheet.getRange(USER_COL.STATUS.col + index).setValue('Already Created.');
            break;
          default:
            sheet.getRange(USER_COL.STATUS.col + index).setValue(errorCode + ': ' + errorMsg);
            break;
        }
      } else {
        sheet.getRange(USER_COL.STATUS.col + index).setValue('Created.');
      }
    });
  }
}

function updateUserInfoWithIndex(sheet, index, __user) {
  var range = sheet.getRange(USER_COL.STATUS.col + index + ':' + USER_COL.ACTION.col + index);
  range.activate();
  
  var _user = {};
  if (__user) {
    _user = __user;
  } else {
    var values = range.getValues();
    _user = createUserByRangeValues(values);
  }
  
  var missingFields = [];
  if (!_user.first_name) missingFields.push('First Name');
  if (!_user.last_name) missingFields.push('Last Name');
  if (!_user.email) missingFields.push('Email');
  if (!_user.password) missingFields.push('Password');
  
  if (missingFields.length > 0) {
    sheet.getRange(USER_COL.STATUS.col + index).setValue('Missing Fields: ' + missingFields.join(', '));
  } else {
    sheet.getRange(USER_COL.STATUS.col + index).setValue('Processing...');
    
    signIn(sheet.getRange('B4').getValue(), _user.email, _user.password, function (response, errorCode, errorMsg) {
      if (errorCode) {
        sheet.getRange(USER_COL.STATUS.col + index).setValue(errorCode + ': ' + errorMsg);
      } else {
        updateOrganizationUser(response.result[0].access_token, response.result[0].id, JSON.stringify({
          'first_name': _user.first_name,
          'last_name': _user.last_name,
          'display_name': _user.display_name,
          'info': _user.info
        }), function (response, errorCode, errorMsg) {
          if (errorCode) {
            sheet.getRange(USER_COL.STATUS.col + index).setValue(errorCode + ': ' + errorMsg + '\n' + JSON.stringify(info));
          } else {
            sheet.getRange(USER_COL.STATUS.col + index).setValue('Edited.');
          }
        });
      }
    });
  }
}

function updateUserTelegramIdWithIndex(sheet, index) {
  var range = sheet.getRange(USER_COL.STATUS.col + index + ':' + USER_COL.ACTION.col + index);
  range.activate();
  
  var values = range.getValues();
  var _user = createUserByRangeValues(values);
  
  var missingFields = [];
  if (!_user.email) missingFields.push('Email');
  if (!_user.password) missingFields.push('Password');
  if (!_user.telegram_id) missingFields.push('Telegram Id');
  
  if (missingFields.length > 0) {
    sheet.getRange(USER_COL.STATUS.col + index).setValue('Missing Fields: ' + missingFields.join(', '));
  } else {
    sheet.getRange(USER_COL.STATUS.col + index).setValue('Processing...');
    
    signIn(sheet.getRange('B4').getValue(), _user.email, _user.password, function (response, errorCode, errorMsg) {
      if (errorCode) {
        sheet.getRange(USER_COL.STATUS.col + index).setValue(errorCode + ': ' + errorMsg);
      } else {
        updateUserTelegramId(response.result[0].access_token, _user.telegram_id, function (response, errorCode, errorMsg) {
          if (errorCode) {
            sheet.getRange(USER_COL.STATUS.col + index).setValue(errorCode + ': ' + errorMsg);
          } else {
            sheet.getRange(USER_COL.STATUS.col + index).setValue('Updated.');
          }
        });
      }
    });
  }
}

function updateUserDingTalkMobileWithIndex(sheet, index) {
  var range = sheet.getRange(USER_COL.STATUS.col + index + ':' + USER_COL.ACTION.col + index);
  range.activate();
  
  var values = range.getValues();
  var _user = createUserByRangeValues(values);
  
  var missingFields = [];
  if (!_user.email) missingFields.push('Email');
  if (!_user.password) missingFields.push('Password');
  if (!_user.ding_talk_mobile) missingFields.push('Mobile (Ding Talk)');
  
  if (missingFields.length > 0) {
    sheet.getRange(USER_COL.STATUS.col + index).setValue('Missing Fields: ' + missingFields.join(', '));
  } else {
    sheet.getRange(USER_COL.STATUS.col + index).setValue('Processing...');
    
    signIn(sheet.getRange('B4').getValue(), _user.email, _user.password, function (response, errorCode, errorMsg) {
      if (errorCode) {
        sheet.getRange(USER_COL.STATUS.col + index).setValue(errorCode + ': ' + errorMsg);
      } else {
        updateUserDingTalk(response.result[0].access_token, _user.ding_talk_mobile, function (response, errorCode, errorMsg) {
          if (errorCode) {
            sheet.getRange(USER_COL.STATUS.col + index).setValue(errorCode + ': ' + errorMsg);
          } else {
            sheet.getRange(USER_COL.STATUS.col + index).setValue('Updated.');
          }
        });
      }
    });
  }
}

// APIs Call with callback

function resultWithCallback(result, callback) {
  var responseJson = JSON.parse(result.getContentText());
  if (result.getResponseCode() == 200 && responseJson.status_code == 'SUCCESS') {
    callback(responseJson, null, null);
  } else if (responseJson.status_code) {
    callback(responseJson, responseJson.status_code, responseJson.msg);
  } else {
    callback(responseJson, result.getResponseCode(), null);
  }
}

// APIs Call

function signIn(org_name, email, password, callback) {
  var params = {
    'org_name': org_name,
    'email': email,
    'password': password
  };
  
  var options = {
    'method': 'post',
    'payload': params
  };
  
  var result = UrlFetchApp.fetch(domain + '/auth/signin', options);
  if (callback) {
    resultWithCallback(result, callback);
  } else {
    return result;
  }
}

function getOrganizationUsers(access_token, callback) {
  var options = {
    'method': 'get',
    'headers': {
      'X-AUTH-TOKEN': access_token
    }
  };
  
  var result = UrlFetchApp.fetch(domain + '/account/organization/user', options);
  if (callback) {
    resultWithCallback(result, callback);
  } else {
    return result;
  }
}

function getResourceTypes(access_token, callback) {
  var options = {
    'method': 'get',
    'headers': {
      'X-AUTH-TOKEN': access_token
    }
  };
  
  var result = UrlFetchApp.fetch(domain + '/resource_type', options);
  if (callback) {
    resultWithCallback(result, callback);
  } else {
    return result;
  }
}

function getUserProfile(access_token, user_id, callback) {
  var options = {
    'method': 'get',
    'headers': {
      'X-AUTH-TOKEN': access_token
    }
  };
  
  var result = UrlFetchApp.fetch(domain + '/account/user_profile/' + user_id, options);
  if (callback) {
    resultWithCallback(result, callback);
  } else {
    return result;
  }
}

function createOrganizationUser(access_token, user, callback) {
  var options = {
    'method': 'post',
    'muteHttpExceptions': true,
    'headers': {
      'X-AUTH-TOKEN': access_token
    },
    'payload': user,
    'contentType': 'application/json'
  };
  
  var result = UrlFetchApp.fetch(domain + '/account/organization/user', options);
  if (callback) {
    resultWithCallback(result, callback);
  } else {
    return result;
  }
}

function updateOrganizationUser(access_token, user_id, user, callback) {
  var options = {
    'method': 'patch',
    'muteHttpExceptions': true,
    'headers': {
      'X-AUTH-TOKEN': access_token
    },
    'payload': user,
    'contentType': 'application/json'
  };
  
  var result = UrlFetchApp.fetch(domain + '/account/user_profile/' + user_id, options);
  if (callback) {
    resultWithCallback(result, callback);
  } else {
    return result;
  }
}

function updateUserTelegramId(access_token, telegram_id, callback) {
  var options = {
    'method': 'patch',
    'muteHttpExceptions': true,
    'headers': {
      'X-AUTH-TOKEN': access_token
    },
    'payload': JSON.stringify({
      'value': telegram_id
    }),
    'contentType': 'application/json'
  };
  
  var result = UrlFetchApp.fetch(domain + '/account/user_profile/account/telegram/chat_id', options);
  if (callback) {
    resultWithCallback(result, callback);
  } else {
    return result;
  }
}

function updateUserDingTalk(access_token, dt_mobile_num, callback) {
  var options = {
    'method': 'patch',
    'muteHttpExceptions': true,
    'headers': {
      'X-AUTH-TOKEN': access_token
    },
    'payload': JSON.stringify({
      'value': dt_mobile_num
    }),
    'contentType': 'application/json'
  };
  
  var result = UrlFetchApp.fetch(domain + '/account/user_profile/account/dingtalk/mobile_number', options);
  if (callback) {
    resultWithCallback(result, callback);
  } else {
    return result;
  }
}

// Convert Role Name -> Role Id

function getRoleId(role_name) {
  switch (role_name) {
    case 'ORGANIZATION_ACCOUNT_ADMIN':
      return '5ac2ff69a44e63c6d572f2f7';
    case 'ORGANIZATION_ACCOUNT_USER':
      return '5ac2ff69a44e63c6d572f2f8';
    case 'ORGANIZATION_ASSET_CREATOR':
      return '5acae37ef8e31a16adfb77e2';
    case 'ORGANIZATION_ASSET_TEMPLATE_CREATOR':
      return '5acae37ef8e31a16adfb77c2';
    case 'ORGANIZATION_ASSET_TEMPLATE_USER':
      return '5acae37ef8e31a16adfb77c3';
    case 'ORGANIZATION_ASSET_TEMPLATE_VIEWER':
      return '5acae37ef8e31a16adfb77c4';
    case 'ORGANIZATION_ASSET_USER':
      return '5acae37ef8e31a16adfb77e3';
    case 'ORGANIZATION_ASSET_VIEWER':
      return '5acae37ef8e31a16adfb77e4';
    case 'ORGANIZATION_CONTRACT_ADMIN':
      return '5acae37ef8e31a16adfb77f1';
    case 'ORGANIZATION_CONTRACT_TEMPLATE_CREATOR':
      return '5acae37ef8e31a16adfb77d2';
    case 'ORGANIZATION_CONTRACT_TEMPLATE_USER':
      return '5acae37ef8e31a16adfb77d3';
    case 'ORGANIZATION_CONTRACT_TEMPLATE_VIEWER':
      return '5acae37ef8e31a16adfb77d4';
    case 'ORGANIZATION_CONTRACT_USER':
      return '5acae37ef8e31a16adfb77f2';
    case 'ORGANIZATION_PROBE_TYPE_CREATOR':
      return '5acae37ef8e31a16adfb77b2';
    case 'ORGANIZATION_PROBE_TYPE_USER':
      return '5acae37ef8e31a16adfb77b3';
    case 'ORGANIZATION_PROBE_TYPE_VIEWER':
      return '5acae37ef8e31a16adfb77b4';
    case 'ORGANIZATION_USER_ADMIN':
      return '5acae37ef8e31a16adfb77a4';
    case 'ORGANIZATION_USER_USER':
      return '5acae37ef8e31a16adfb77a5';
    case 'SYSTEM_ACCOUNT_ADMIN':
      return '5ac2ff69a44e63c6d572f2f5';
    case 'SYSTEM_ACCOUNT_USER':
      return '5ac2ff69a44e63c6d572f2f6';
    case 'SYSTEM_ASSET_CREATOR':
      return '5acae37ef8e31a16adfb77d8';
    case 'SYSTEM_ASSET_TEMPLATE_CREATOR':
      return '5acae37ef8e31a16adfb77b8';
    case 'SYSTEM_ASSET_TEMPLATE_USER':
      return '5acae37ef8e31a16adfb77b9';
    case 'SYSTEM_ASSET_TEMPLATE_VIEWER':
      return '5acae37ef8e31a16adfb77c1';
    case 'SYSTEM_ASSET_USER':
      return '5acae37ef8e31a16adfb77d9';
    case 'SYSTEM_ASSET_VIEWER':
      return '5acae37ef8e31a16adfb77e1';
    case 'SYSTEM_CONTRACT_ADMIN':
      return '5acae37ef8e31a16adfb77e8';
    case 'SYSTEM_CONTRACT_TEMPLATE_CREATOR':
      return '5acae37ef8e31a16adfb77c8';
    case 'SYSTEM_CONTRACT_TEMPLATE_USER':
      return '5acae37ef8e31a16adfb77c9';
    case 'SYSTEM_CONTRACT_TEMPLATE_VIEWER':
      return '5acae37ef8e31a16adfb77d1';
    case 'SYSTEM_CONTRACT_USER':
      return '5acae37ef8e31a16adfb77e9';
    case 'SYSTEM_PROBE_TYPE_CREATOR':
      return '5acae37ef8e31a16adfb77a8';
    case 'SYSTEM_PROBE_TYPE_USER':
      return '5acae37ef8e31a16adfb77a9';
    case 'SYSTEM_PROBE_TYPE_VIEWER':
      return '5acae37ef8e31a16adfb77b1';
    case 'SYSTEM_USER_ADMIN':
      return '5acae37ef8e31a16adfb77a2';
    case 'SYSTEM_USER_USER':
      return '5acae37ef8e31a16adfb77a3';
    case 'WORKGROUP_ACCOUNT_ADMIN':
      return '5ac2ff69a44e63c6d572f2f9';
    case 'WORKGROUP_ACCOUNT_USER':
      return '5acae37ef8e31a16adfb77a1';
    case 'WORKGROUP_ASSET_CREATOR':
      return '5acae37ef8e31a16adfb77e5';
    case 'WORKGROUP_ASSET_TEMPLATE_CREATOR':
      return '5acae37ef8e31a16adfb77c5';
    case 'WORKGROUP_ASSET_TEMPLATE_USER':
      return '5acae37ef8e31a16adfb77c6';
    case 'WORKGROUP_ASSET_TEMPLATE_VIEWER':
      return '5acae37ef8e31a16adfb77c7';
    case 'WORKGROUP_ASSET_USER':
      return '5acae37ef8e31a16adfb77e6';
    case 'WORKGROUP_ASSET_VIEWER':
      return '5acae37ef8e31a16adfb77e7';
    case 'WORKGROUP_CONTRACT_ADMIN':
      return '5acae37ef8e31a16adfb77f3';
    case 'WORKGROUP_CONTRACT_TEMPLATE_CREATOR':
      return '5acae37ef8e31a16adfb77d5';
    case 'WORKGROUP_CONTRACT_TEMPLATE_USER':
      return '5acae37ef8e31a16adfb77d6';
    case 'WORKGROUP_CONTRACT_TEMPLATE_VIEWER':
      return '5acae37ef8e31a16adfb77d7';
    case 'WORKGROUP_CONTRACT_USER':
      return '5acae37ef8e31a16adfb77f4';
    case 'WORKGROUP_PROBE_TYPE_CREATOR':
      return '5acae37ef8e31a16adfb77b5';
    case 'WORKGROUP_PROBE_TYPE_USER':
      return '5acae37ef8e31a16adfb77b6';
    case 'WORKGROUP_PROBE_TYPE_VIEWER':
      return '5acae37ef8e31a16adfb77b7';
    case 'WORKGROUP_USER_ADMIN':
      return '5acae37ef8e31a16adfb77a6';
    case 'WORKGROUP_USER_USER':
      return '5acae37ef8e31a16adfb77a7';
  }
}

function createUserByRangeValues(values) {
  var info = [];
  var filteredResTypes = resourceTypes.filter(function(resourceType) {
    return resourceType.type == RES_TYPE.TELEGRAM_CHAT_ID.type;
  });
  if (filteredResTypes.length > 0) {
    info.push({
      'resource_type_id': filteredResTypes[0].id,
      'value': values[0][USER_COL.TELEGRAM_ID.idx].toString()
    })
  }
  filteredResTypes = resourceTypes.filter(function(resourceType) {
    return resourceType.type == RES_TYPE.DINGTALK_PHONE_NUMBER.type;
  });
  if (filteredResTypes.length > 0) {
    info.push({
      'resource_type_id': filteredResTypes[0].id,
      'value': values[0][USER_COL.MOBILE_DING_TALK.idx].toString()
    })
  }
  
  return {
    first_name: values[0][USER_COL.FIRST_NAME.idx].toString(),
    last_name: values[0][USER_COL.LAST_NAME.idx].toString(),
    username: values[0][USER_COL.USERNAME.idx].toString(),
    email: values[0][USER_COL.EMAIL.idx].toString(),
    password: values[0][USER_COL.PASSWORD.idx].toString(),
    display_name: values[0][USER_COL.FIRST_NAME.idx].toString() + ' ' + values[0][USER_COL.LAST_NAME.idx].toString(),
    telegram_id: values[0][USER_COL.TELEGRAM_ID.idx].toString(),
    ding_talk_mobile: values[0][USER_COL.MOBILE_DING_TALK.idx].toString(),
    info: info,
    role_ids: values[0][USER_COL.ROLE.idx].toString() == 'Admin' ?
    [
    getRoleId('ORGANIZATION_ACCOUNT_USER'),
    getRoleId('ORGANIZATION_ASSET_CREATOR'),
    getRoleId('ORGANIZATION_ASSET_TEMPLATE_CREATOR'),
    getRoleId('ORGANIZATION_ASSET_TEMPLATE_USER'),
    getRoleId('ORGANIZATION_ASSET_TEMPLATE_VIEWER'),
    getRoleId('ORGANIZATION_ASSET_USER'),
    getRoleId('ORGANIZATION_ASSET_VIEWER'),
    getRoleId('ORGANIZATION_CONTRACT_ADMIN'),
    getRoleId('ORGANIZATION_CONTRACT_TEMPLATE_CREATOR'),
    getRoleId('ORGANIZATION_CONTRACT_TEMPLATE_USER'),
    getRoleId('ORGANIZATION_CONTRACT_TEMPLATE_VIEWER'),
    getRoleId('ORGANIZATION_CONTRACT_USER'),
    getRoleId('ORGANIZATION_PROBE_TYPE_USER'),
    getRoleId('ORGANIZATION_PROBE_TYPE_VIEWER'),
    getRoleId('ORGANIZATION_USER_ADMIN'),
    getRoleId('ORGANIZATION_USER_USER'),
    getRoleId('SYSTEM_ASSET_TEMPLATE_USER'),
    getRoleId('SYSTEM_ASSET_TEMPLATE_VIEWER'),
    getRoleId('SYSTEM_CONTRACT_TEMPLATE_USER'),
    getRoleId('SYSTEM_CONTRACT_TEMPLATE_VIEWER'),
    getRoleId('SYSTEM_PROBE_TYPE_USER'),
    getRoleId('SYSTEM_PROBE_TYPE_VIEWER')
    ] :
    [
    getRoleId('ORGANIZATION_ACCOUNT_USER'),
    getRoleId('ORGANIZATION_USER_USER')
    ],
    destination: user.destination
  };
}